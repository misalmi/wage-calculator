import Dependencies._

ThisBuild / scalaVersion     := "2.12.8"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.paymenthighway"
ThisBuild / organizationName := "paymenthighway"
ThisBuild / logBuffered in Test := false

lazy val root = (project in file("."))
  .settings(
    name := "wagecalculator",
    libraryDependencies ++= Seq(
      scalaTest % Test,
      "com.nrinaudo" %% "kantan.csv" % "0.5.0",
      "com.nrinaudo" %% "kantan.csv-joda-time" % "0.5.0",
      "com.nrinaudo" %% "kantan.csv-generic" % "0.5.0",
      "joda-time" % "joda-time" % "2.9.3",
      "org.scalactic" %% "scalactic" % "3.0.5",
      "org.scalatest" %% "scalatest" % "3.0.5" % "test"
    )
  )
