package wagecalculator

import org.scalatest._
import org.joda.time._

class WageCalculatorSpec extends FlatSpec with Matchers {
  val defaultName = "Harry"
  val defaultId = 1
  val defaultDay = 1
  val defaultMonth = 1
  val defaultYear = 2019

  def makeLocalDate(day: Int, month: Int = defaultMonth): LocalDate =
    new LocalDate(defaultYear, month, day)

  val defaultMonthBeginning = makeLocalDate(defaultDay)

  def makeLocalTime(hour: Int): LocalTime =
    new LocalTime(hour, 0)

  def makeDefaultShift(day: Int, start: Int, end: Int): WorkShift =
    WorkShift(
        defaultName,
        defaultId,
        makeLocalDate(day),
        makeLocalTime(start),
        makeLocalTime(end)
    )

  def makeExpectedMap(expectedWage: Double): Map[LocalDate, Map[Int, WageByName]] =
    Map(defaultMonthBeginning -> Map(defaultId -> WageByName(defaultName, expectedWage)))

  def getWage(retVal: Map[LocalDate, Map[Int, WageByName]]): Double =
    retVal.get(defaultMonthBeginning).get(defaultId).wage

  "WageCalculator" should "calculate monthly wage map" in {
    val calc = new WageCalculator
    val shifts = List(makeDefaultShift(defaultDay, 6, 14))
    val expectedWage = 30.0
    val expectedVal = makeExpectedMap(expectedWage)
    calc.calculateWages(shifts) shouldEqual expectedVal
  }

  "WageCalculator" should "sum two wages" in {
    val calc = new WageCalculator
    val secondDay = 5
    val shifts = List(
      makeDefaultShift(defaultDay, 6, 14),
      makeDefaultShift(secondDay, 6, 14)
    )
    val expectedWage = 60.0
    val expectedVal = makeExpectedMap(expectedWage)
    calc.calculateWages(shifts) shouldEqual expectedVal
  }

  "WageCalculator" should "include overtime compensation to wage" in {
    val calc = new WageCalculator
    val shifts = List(
      makeDefaultShift(defaultDay, 7, 16)
    )
    val expectedWage = 34.6875
    getWage(calc.calculateWages(shifts)) shouldEqual expectedWage
  }

  "WageCalculator" should "increase overtime compensation after 2h" in {
    val calc = new WageCalculator
    val shifts = List(
      makeDefaultShift(defaultDay, 7, 18)
    )
    val expectedWage = 45
    getWage(calc.calculateWages(shifts)) shouldEqual expectedWage
  }

  "WageCalculator" should "increase overtime compensation again after 2h (+ evening hour)" in {
    val calc = new WageCalculator
    val shifts = List(
      makeDefaultShift(defaultDay, 6, 19)
    )
    val expectedWage = 59.275
    getWage(calc.calculateWages(shifts)) shouldEqual expectedWage
  }

  "WageCalculator" should "calculate overtime correctly for two shifts on a same day" in {
    val calc = new WageCalculator
    val shifts = List(
      makeDefaultShift(defaultDay, 7, 13),
      makeDefaultShift(defaultDay, 15, 18)
    )
    val expectedWage = 34.6875
    getWage(calc.calculateWages(shifts)) shouldEqual expectedWage
  }

  "WageCalculator" should "calculate evening compensation for evening and morning" in {
    val calc = new WageCalculator
    val shifts = List(
      makeDefaultShift(defaultDay, 4, 8),
      makeDefaultShift(defaultDay, 16, 20)
    )
    val expectedWage = 34.6
    getWage(calc.calculateWages(shifts)) shouldEqual expectedWage
  }

  "WageCalculator" should "calculate evening compensation for overnight shift" in {
    val calc = new WageCalculator
    val shifts = List(
      makeDefaultShift(defaultDay, 22, 4)
    )
    val expectedWage = 29.4
    getWage(calc.calculateWages(shifts)) shouldEqual expectedWage
  }

  "WageCalculator" should "calculate compensation correctly for 23 hour shift" in {
    val calc = new WageCalculator
    val shifts = List(
      makeDefaultShift(defaultDay, 23, 22)
    )
    val expectedWage = 145.775
    getWage(calc.calculateWages(shifts)) shouldEqual expectedWage
  }

  "WageCalculator" should "take separate months into account" in {
    val calc = new WageCalculator
    val secondMonth = 2
    val shifts = List(
      makeDefaultShift(defaultDay, 6, 14),
      WorkShift(
        defaultName,
        defaultId,
        makeLocalDate(defaultDay, secondMonth),
        makeLocalTime(6),
        makeLocalTime(14)
      )
    )
    val expectedVal = Map(
      defaultMonthBeginning -> Map(defaultId -> WageByName(defaultName, 30.0)),
      makeLocalDate(1, secondMonth) -> Map(defaultId -> WageByName(defaultName , 30.0))
    )
    calc.calculateWages(shifts) shouldEqual expectedVal
  }

  "WageCalculator" should "take separate employees into account" in {
    val calc = new WageCalculator
    val secondUserName = "Hermione"
    val secondUserId = 2
    val shifts = List(
      makeDefaultShift(defaultDay, 6, 14),
      WorkShift(
        secondUserName,
        secondUserId,
        makeLocalDate(defaultDay, defaultMonth),
        makeLocalTime(6),
        makeLocalTime(14)
      )
    )
    val expectedVal = Map(defaultMonthBeginning -> Map(
      defaultId -> WageByName(defaultName, 30.0),
      secondUserId -> WageByName(secondUserName, 30.0)
    ))
    calc.calculateWages(shifts) shouldEqual expectedVal
  }

  "WageCalculator" should "calculate wages correctly" in {
    val calc = new WageCalculator
    val secondUserName = "Hermione"
    val secondUserId = 2
    val thirdUserName = "Ron"
    val thirdUserId = 3
    val secondDay = 5
    val secondMonth = 2
    val shifts = List(
      makeDefaultShift(defaultDay, 4, 8),
      makeDefaultShift(defaultDay, 16, 20),
      WorkShift(
        secondUserName,
        secondUserId,
        makeLocalDate(defaultDay, defaultMonth),
        makeLocalTime(23),
        makeLocalTime(22)
      ),
      WorkShift(
        secondUserName,
        secondUserId,
        makeLocalDate(secondDay, defaultMonth),
        makeLocalTime(22),
        makeLocalTime(4)
      ),
      WorkShift(
        thirdUserName,
        thirdUserId,
        makeLocalDate(defaultDay, secondMonth),
        makeLocalTime(7),
        makeLocalTime(18)
      ),
      WorkShift(
        thirdUserName,
        thirdUserId,
        makeLocalDate(secondDay, secondMonth),
        makeLocalTime(6),
        makeLocalTime(19)
      )
    )

    val expectedVal = Map(
      defaultMonthBeginning -> Map(
        defaultId -> WageByName(defaultName, 34.6),
        secondUserId -> WageByName(secondUserName, 175.175)
      ),
      makeLocalDate(defaultDay, 2) -> Map(
        thirdUserId -> WageByName(thirdUserName, 104.275)
      )
    )
    calc.calculateWages(shifts) shouldEqual expectedVal
  }
}
