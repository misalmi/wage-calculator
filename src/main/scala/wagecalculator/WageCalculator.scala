package wagecalculator

import scala.io.Source
import scala.util.Try
import java.io.IOException
import kantan.csv._
import kantan.csv.ops._
import kantan.csv.generic._
import kantan.csv.joda.time._
import org.joda.time._

case class WorkShift(
  name: String,
  id: Int,
  date: LocalDate,
  start: LocalTime,
  end: LocalTime
)

case class WageByName(
  name: String,
  wage: Double
)

class WageCalculator {
  val MinutesInHour = 60.0
  val HourlyWage = 3.75
  val EveningCompensation = 1.15
  val RegularDailyHours = 8
  val FullOverTimeHours = 12
  val OvertimeInterval = 2
  val FirstMinuteOfDay = new LocalTime(0, 0)
  val LastMinuteOfDay = new LocalTime(23, 59)
  val EveningHoursStart = new LocalTime(18, 0)
  val EveningHoursEnd = new LocalTime(6, 0)

  private[this] def diffFromEndOfDay(t: LocalTime) =
    diffTimeInHours(t, LastMinuteOfDay) + 1 / MinutesInHour

  private[this] def diffTimeInHours(start: LocalTime, end: LocalTime): Double = {
    val diff = new Period(start, end)
    diff.getHours + diff.getMinutes / MinutesInHour
  }

  private[this] def diffTimeOvernight(start: LocalTime, end: LocalTime): Double =
    diffFromEndOfDay(start) + diffTimeInHours(FirstMinuteOfDay, end)

  private[this] def calculateDailyHours(start: LocalTime, end: LocalTime): Double =
    if (start.isBefore(end))
      diffTimeInHours(start, end)
    else
      diffTimeOvernight(start, end)

  private[this] def calculateDailyWage(hours: Double): Double =
    hours * HourlyWage

  private[this] def calculateOvertimeCompensation(
    hours: Double,
    wage: Double = HourlyWage,
    overtime: Double = FullOverTimeHours
  ): Double =
    if (hours <= RegularDailyHours) 0 else {
      val overtimeExceeded = hours > overtime
      val compensation = if (overtimeExceeded) (hours - overtime) * wage else 0
      compensation + calculateOvertimeCompensation(
        if (overtimeExceeded) overtime else hours,
        wage / 2,
        overtime - OvertimeInterval
      )
    }

  private[this] def calculateEveningCompensation(shift: WorkShift): Double = {
    val (start, end) = (shift.start, shift.end)
    val startAfterRegularHours = start.isAfter(EveningHoursStart)
    val endBeforeRegularHours = end.isBefore(EveningHoursEnd)

    def calculateMorningHours() = {
      val startBeforeRegularHours = start.isBefore(EveningHoursEnd)
      if (startBeforeRegularHours) diffTimeInHours(start, EveningHoursEnd) else 0
    }

    def calculateEveningHours() = {
      val endAfterRegularHours = end.isAfter(EveningHoursStart)
      if (endAfterRegularHours) diffTimeInHours(EveningHoursStart, end) else 0
    }

    def calculateNoOverNight() =
      if (endBeforeRegularHours || startAfterRegularHours)
        diffTimeInHours(start, end)
      else
        calculateMorningHours + calculateEveningHours

    def calculateFirstDayOverNight() =
      if (startAfterRegularHours)
        diffFromEndOfDay(start)
      else
        calculateMorningHours + diffFromEndOfDay(EveningHoursStart)

    def calculateSecondDayOverNight() =
      if (endBeforeRegularHours)
        diffTimeInHours(FirstMinuteOfDay, end)
      else
        calculateEveningHours + diffTimeInHours(FirstMinuteOfDay, EveningHoursEnd)

    def calculateOverNight() =
      if (startAfterRegularHours && endBeforeRegularHours)
        diffTimeOvernight(start, end)
      else
        calculateFirstDayOverNight + calculateSecondDayOverNight

    val overnight = start.isAfter(end)
    val hours = if (overnight) calculateOverNight else calculateNoOverNight
    hours * EveningCompensation
  }

  private[this] def calculateHoursPerDay(shifts: Seq[WorkShift]): Iterable[Double] =
    shifts.groupBy(_.date).mapValues(_.foldLeft(0: Double){(acc, shift) =>
      acc + calculateDailyHours(shift.start, shift.end)
    }).values

  private[this] def calculateWagePerMonth(shifts: Seq[WorkShift]): Double = {
    val dailyWageWithOT = calculateHoursPerDay(shifts)
      .foldLeft(0: Double) {(acc, hours) =>
        acc + calculateDailyWage(hours) + calculateOvertimeCompensation(hours)
      }
    val eveningCompensation = shifts.foldLeft(0: Double) {(acc, shift) =>
      acc + calculateEveningCompensation(shift)
    }
    dailyWageWithOT + eveningCompensation
  }

  private[this] def groupShiftsByMonth(shifts: Seq[WorkShift]): Map[LocalDate, Seq[WorkShift]] =
    shifts.groupBy(shift => {
      val date = shift.date
      new LocalDate(date.getYear, date.getMonthOfYear, 1)
    })

  def calculateWages(shifts: Seq[WorkShift]): Map[LocalDate, Map[Int, WageByName]] = {
    groupShiftsByMonth(shifts).mapValues(shiftsByMonth => {
      val shiftsById = shiftsByMonth.groupBy(_.id)
      shiftsById.mapValues(s => WageByName(s(0).name, calculateWagePerMonth(s)))
    })
  }
}

object WageCalculator extends App {
  def readCsv(data: String): Either[ReadError, List[WorkShift]] = {
    implicit val dateDecoder = localDateDecoder(fmt"dd.MM.yyyy")
    implicit val timeDecoder = localTimeDecoder(fmt"HH:mm")
    ReadResult.sequence(data.readCsv[List, WorkShift](rfc.withHeader))
  }

  def readFileToString(path: String): Option[String] =
    Try(Source.fromFile(path)).fold(_ => None, {f =>
      val content = Try(f.mkString)
      f.close
      content.toOption
    })

  def printResult(result: Map[LocalDate, Map[Int, WageByName]]): Unit =
    result.foreach { case (monthDate, wages) => {
      println(f"Monthly wages ${monthDate.getMonthOfYear}%02d/${monthDate.getYear}:")
      wages.foreach { case (id, wageByName) =>
        println(f"${wageByName.name} ${wageByName.wage}%1.2f$$") }
    }}

  override def main(args: Array[String]): Unit =
    if (args.length < 1) println("Provide filepath as first argument")
    else
      readFileToString(args(0)) match {
        case Some(s) => readCsv(s) match {
          case Right(shifts) => printResult((new WageCalculator).calculateWages(shifts))
          case Left(e) => println(e.getMessage)
        }
        case None => println("Failed to read the file")
      }
}
